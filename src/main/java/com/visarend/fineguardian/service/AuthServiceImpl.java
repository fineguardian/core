package com.visarend.fineguardian.service;

import com.visarend.fineguardian.dto.AuthResponse;
import com.visarend.fineguardian.exception.AppAuthenticationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Service
public class AuthServiceImpl implements AuthService {
    @Value("${fineguardian.keycloak.auth-client-id}")
    private String clientId;
    @Value("${fineguardian.keycloak.login-url}")
    private String loginUrl;

    @Autowired
    WebClient.Builder webClientBuilder;

    @Override
    public Mono<AuthResponse> login(String username, String password) {
        WebClient webClient = webClientBuilder.build();
        MultiValueMap<String, String> formData = new LinkedMultiValueMap<>();
        formData.add("client_id", clientId);
        formData.add("grant_type", "password");
        formData.add("username", username);
        formData.add("password", password);

        return webClient.post()
                .uri(loginUrl)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                .bodyValue(formData)
                .retrieve()
                .bodyToMono(AuthResponse.class)
                .onErrorMap(e -> new AppAuthenticationException(e.getMessage()));
    }


}
