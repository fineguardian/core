package com.visarend.fineguardian.service;


import com.visarend.fineguardian.dto.AuthRequest;
import com.visarend.fineguardian.dto.AuthResponse;
import reactor.core.publisher.Mono;

public interface AuthService {
    Mono<AuthResponse> login(String username, String password);
}
