package com.visarend.fineguardian.web;

import com.visarend.fineguardian.dto.AuthRequest;
import com.visarend.fineguardian.dto.AuthResponse;
import com.visarend.fineguardian.service.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
@RestController
@RequestMapping("/auth")
public class AuthController {

    private final AuthService service;

    @PostMapping("/login")
    public Mono<AuthResponse> login(@RequestBody AuthRequest request) {
        return service.login(request.getUsername(), request.getPassword());
    }
}
