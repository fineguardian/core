package com.visarend.fineguardian.exception;

public class AppAuthenticationException extends RuntimeException{
    public AppAuthenticationException(String message) {
        super(message);
    }

    @Override
    public synchronized Throwable fillInStackTrace() {
        return this;
    }

}
