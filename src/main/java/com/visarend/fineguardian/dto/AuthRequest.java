package com.visarend.fineguardian.dto;

import jakarta.annotation.Nonnull;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Builder
@Data
@RequiredArgsConstructor
public class AuthRequest {
    @Nonnull
    private String username;
    @Nonnull
    private String password;
}
